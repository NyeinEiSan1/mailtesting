<?php

namespace App\Http\Controllers;
use App\Models\Post;

use App\Mail\DemoMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function index(Request $request){
       $post=new Post;
       $post->mail=$request->mail;
       $post->title=$request->title;
       $post->body=$request->body;
        $mailData=['title'=>$post->title,
                    'body'=>$post->body];
        Mail::to($post->mail)->send(new DemoMail($mailData));
        return response("Mail sent successfully");
    }


}
